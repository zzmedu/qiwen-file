//package com.mac.scp.controller;
//
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.ResponseBody;
//import org.springframework.web.bind.annotation.RestController;
//import org.springframework.web.servlet.ModelAndView;
//
///**
// * 用户控制类
// *
// * @author ma116
// */
//@RestController
//public class RouterController {
//
//
//    @RequestMapping("/file")
//    @ResponseBody
//    public ModelAndView file() {
//        return new ModelAndView("/file.html");
//    }
//
//    @RequestMapping("/login")
//    @ResponseBody
//    public ModelAndView login() {
//        return new ModelAndView("/login");
//    }
//
//    @RequestMapping("/register")
//    @ResponseBody
//    public ModelAndView register() {
//        return new ModelAndView("/register");
//    }
//
//}
